export default {
  // image addresses and alternative text
  TESUTO_LOGO: '/images/logo-white.svg',
  TESUTO_LOGO_ALT: 'Tesuto Logo',

  // elements height and width
  HEADER_HEIGHT: '68px',

  // common text sizes
  H1: '24px'
}
