import React, { Component } from 'react'
import * as styles from 'constants/styles'
import colors from '../../constants/colors'

class EmulationSelector extends Component {
  render() {
    return (
      <div
        style={{
          ...styles.common.flex_row_center,
          backgroundColor: colors.gray
        }}
      >
        {this.props.emulations.map(emulation => {
          let selected = emulation.id === this.props.selected_emultaion
          return (
            <div
              // todo: refactor styles
              style={{
                margin: '10px',
                padding: '10px',
                backgroundColor: selected ? colors.blue_dark : colors.white,
                color: selected ? colors.white : colors.blue_dark,
                cursor: 'pointer'
              }}
              onClick={() => this._onEmulationClick(emulation)}
            >
              {emulation.name}
            </div>
          )
        })}
      </div>
    )
  }

  _onEmulationClick = emulation => {
    this.props.onEmulationClick(emulation)
  }
}

EmulationSelector.propTypes = {}

export default EmulationSelector
