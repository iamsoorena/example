import Header from './Header'
import React from 'react'

// components by page
import Home from './Home'
import Login from './Login'

//redux related imports
import { connect } from 'react-redux'
import { REGISTER_TOKEN } from 'constants/actionTypes'
import { Route, Switch } from 'react-router-dom'

const mapStateToProps = state => {
  return {
    token: state.common.token
  }
}

const mapDispatchToProps = dispatch => ({
  registerToken: token => dispatch({ type: REGISTER_TOKEN, payload: token })
})

class App extends React.Component {
  render() {
    if (this.props.token) {
      return (
        <div>
          <Header />
          <Switch>
            <Route exact path="/" component={Home} />
          </Switch>
        </div>
      )
    }
    return (
      <div>
        <Login registerToken={this.props.registerToken} />
      </div>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
