import React, { Component } from 'react'
import values from 'constants/values'
import { logo as styles } from 'constants/styles'
import Proptypes from 'prop-types'

class Logo extends Component {
  render() {
    return (
      <div>
        <img
          src={values.TESUTO_LOGO}
          alt={values.TESUTO_LOGO_ALT}
          style={{ ...styles, width: this.props.size || '100px' }}
        />
      </div>
    )
  }
}

Logo.propTypes = {
  size: Proptypes.string
}

export default Logo
