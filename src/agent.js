import superagentPromise from 'superagent-promise'
import _superagent from 'superagent'

const superagent = superagentPromise(_superagent, global.Promise)

const API_ROOT = 'https://api.tesuto.com/v1'

const responseBody = res => res.body

let token = null

const headerPlugin = req => {
  if (token) {
    req.set('Authorization', `Bearer ${token}`)
    req.set('Cache-Control', 'no-cache')
    req.set('Content-Type', 'application/json')
  }
}

const requests = {
  get: url =>
    superagent
      .get(`${API_ROOT}${url}`)
      .use(headerPlugin)
      .then(responseBody),

  // these methods currently have no use.
  del: url =>
    superagent
      .del(`${API_ROOT}${url}`)
      .use(headerPlugin)
      .then(responseBody),
  put: (url, body) =>
    superagent
      .put(`${API_ROOT}${url}`, body)
      .use(headerPlugin)
      .then(responseBody),
  post: (url, body) =>
    superagent
      .post(`${API_ROOT}${url}`, body)
      .use(headerPlugin)
      .then(responseBody)
}

const Topology = {
  getTopologies: emulation_id =>
    requests.get(`/emulations/${emulation_id}/topologies`),
  getEmulations: () => requests.get('/emulations'),
  getDevices: emulation_id =>
    requests.get(`/emulations/${emulation_id}/devices`)
}

export default {
  Topology,
  setToken: _token => (token = _token)
}
