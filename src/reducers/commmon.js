import { APP_LOAD, REGISTER_TOKEN, SET_EMULATION } from 'constants/actionTypes'
import agent from 'agent'

const defaultState = {
  appName: 'Tesuto',
  token: null
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case APP_LOAD:
      return {
        ...state,
        appLoaded: true
      }
    case REGISTER_TOKEN:
      agent.setToken(action.payload)
      return {
        ...state,
        token: action.error ? null : action.payload
      }
    case SET_EMULATION:
      return {
        ...state,
        appLoaded: false
      }
    default:
      return state
  }
}
