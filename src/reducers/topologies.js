import {
  LOAD_TOPOLOGY_DATA,
  LOAD_DEVICES_DATA,
  LOAD_EMULATIONS_DATA,
  SET_EMULATION
} from '../constants/actionTypes'

export default (state = { emulations: [] }, action) => {
  switch (action.type) {
    case LOAD_TOPOLOGY_DATA:
      return {
        ...state,
        topologies: action.payload
      }
    case LOAD_DEVICES_DATA:
      return {
        ...state,
        devices: action.payload && action.payload.data
      }
    case LOAD_EMULATIONS_DATA:
      return {
        ...state,
        emulations: action.payload
      }
    case SET_EMULATION:
      return {
        ...state,
        selected_emulation: state.emulations.find(
          e => e.name === action.payload.name
        ).id
      }
    default:
      return state
  }
}
